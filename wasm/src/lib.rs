use wasm_bindgen::prelude::*;
use base58::{ToBase58, FromBase58};
use base64::engine::{Engine as _, general_purpose};

use kyber_rs::{
    encoding::BinaryUnmarshaler,
    encrypt::encrypt,
    group::edwards25519::{Point, SuiteEd25519},
};
use sha2::{Sha256, Digest};
use crypto_box::{
    seal, seal_open,
    PublicKey, SecretKey, KEY_SIZE
};
use rand::rngs::OsRng;

const ERR_BASE58: &str = "ERR_BASE58";
const ERR_BASE64: &str = "ERR_BASE64";
const ERR_SERDE: &str = "ERR_SERDE";
const ERR_KYBER: &str = "ERR_KYBER";
const ERR_CRYPTO: &str = "ERR_CRYPTO";
const ERR_INPUT: &str = "ERR_INPUT";

#[allow(unused)]
#[wasm_bindgen]
pub struct EncryptedBallot {
    encrypted_ballot: String,
    encrypted_telling_token: String,
}

#[wasm_bindgen]
impl EncryptedBallot {
    #[wasm_bindgen(getter)]
    pub fn encrypted_ballot(&self) -> String {
        self.encrypted_ballot.clone()
    }
    
    #[wasm_bindgen(getter)]
    pub fn encrypted_telling_token(&self) -> String {
        self.encrypted_telling_token.clone()
    }
}

#[wasm_bindgen]
pub fn encrypt_ballot(ballot_json: String, vote_key_str: String, telling_token_json: String, tellers_key: String) -> Result<EncryptedBallot, String> {
    let vote_key_vec = vote_key_str.from_base58().map_err(|_| ERR_BASE58)?;
    let tellers_key_vec = tellers_key.from_base58().map_err(|_| ERR_BASE58)?;
    let tellers_key_arr: [u8; KEY_SIZE] = tellers_key_vec.try_into().map_err(|_| ERR_INPUT)?;

    let mut vote_key: Point = Point::default();
    vote_key.unmarshal_binary(&vote_key_vec).map_err(|_| ERR_SERDE)?;
    let ballot_bytes: Vec<u8> = ballot_json.bytes().collect();
    let ballot_cipher = encrypt(SuiteEd25519::default(), vote_key, &ballot_bytes).map_err(|_| ERR_KYBER)?;

    let mut rng = OsRng::default();
    let telling_token_bytes: Vec<u8> = telling_token_json.bytes().collect();
    let tellers_pubkey = PublicKey::from(tellers_key_arr);
    let telling_token_cipher = seal(&mut rng, &tellers_pubkey, &telling_token_bytes).map_err(|_| ERR_CRYPTO)?;

    Ok(EncryptedBallot {
        encrypted_ballot: general_purpose::STANDARD.encode(ballot_cipher),
        encrypted_telling_token: general_purpose::STANDARD.encode(telling_token_cipher)
    })
}

#[wasm_bindgen]
pub fn random_seed() -> Result<String, String> {
    let mut arr: [u8; 32] = [0; 32]; 
    getrandom::getrandom(&mut arr).map_err(|_| ERR_CRYPTO)?;
    Ok(arr.to_base58())
}

#[wasm_bindgen]
pub fn hash(s: String) -> String {
    let mut hasher = Sha256::new();
    let bytes: Vec<u8> = s.bytes().collect();
    hasher.update(&bytes);
    hasher.finalize().to_base58()
}

#[allow(unused)]
#[wasm_bindgen]
pub struct TellersKeyPair {
    pubkey: String,
    key: String,
}

#[wasm_bindgen]
impl TellersKeyPair {
    #[wasm_bindgen(getter)]
    pub fn pubkey(&self) -> String {
        self.pubkey.clone()
    }

    #[wasm_bindgen(getter)]
    pub fn key(&self) -> String {
        self.key.clone()
    }
}

#[wasm_bindgen]
pub fn generate_tellers_pair() -> TellersKeyPair {
    let mut rng = OsRng::default();
    let secret_key = SecretKey::generate(&mut rng);
    let public_key = secret_key.public_key();

    TellersKeyPair { pubkey: public_key.as_bytes().to_base58(), key: secret_key.as_bytes().to_base58() }
}

#[wasm_bindgen]
pub fn teller_decryption(cipher: String, tellers_key: String) -> Result<String, String> {
    let telling_token_enc = general_purpose::STANDARD.decode(cipher).map_err(|_| ERR_BASE64)?;

    let tellers_key_arr: [u8; KEY_SIZE] = tellers_key.from_base58().map_err(|_| ERR_BASE58)?.try_into().map_err(|_| ERR_INPUT)?;
    let tellers_key = SecretKey::from(tellers_key_arr);

    match seal_open(&tellers_key, &telling_token_enc) {
        Ok(decrypted_token) => String::from_utf8(decrypted_token).map_err(|_| ERR_INPUT.into()),
        Err(_) => Err(ERR_CRYPTO.into()),
    }
}