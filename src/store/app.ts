// Utilities
import type { AppError, AppStore } from '@/scripts/types'
import { defineStore } from 'pinia'

export const useAppStore = defineStore('app', {
  state: (): AppStore => ({
    // Display
    navigationDrawer: window.innerWidth > 1024,
    isDesktop: window.innerWidth > 1024, // tailwind lg breakpoint

    // Messages and errors
    globalMessageModal: false,
    globalMessageError: null,
    globalMessageInfo: null,
    globalMessageLoading: false,

    // Snackbar
    globalSnackbarDisplayed: false,
    globalSnackbarText: '',
    globalSnackbarType: 'info',
    globalNewVote: false,
    globalNewVoteIndex: null
  }),
  actions: {
    toggleDrawer (value: boolean | null) {
      this.navigationDrawer = !this.navigationDrawer
    },
    displayError (error: AppError, additionalInfo?: string) {
      this.globalMessageError = error
      this.globalMessageInfo = additionalInfo ?? null
      this.globalMessageModal = true
    },
    clearMessage () {
      this.globalMessageModal = false
      this.globalMessageError = null
      this.globalMessageInfo = null
    },
    notification (type: 'info' | 'error' | 'success', text: string) {
      this.globalSnackbarDisplayed = true
      this.globalSnackbarType = type
      this.globalSnackbarText = text
    },
    newOpenVoteNotif (voteIndex: number) {
      const savedBallotsStr = localStorage.getItem('saved-ballots')
      if (savedBallotsStr) {
        try {
          const savedBallots = JSON.parse(savedBallotsStr)
          const ballot = savedBallots[voteIndex]
          if (ballot) {
            console.log("Silencing newOpenVoteNotif, ballot found :", JSON.stringify(ballot))
            return
          }
        } catch (e) {
          console.error('Error thrown while decoding savedBallots', e)
        }
      }

      this.globalNewVoteIndex = voteIndex
      this.globalNewVote = true
    },
    clearNewOpenVoteNotif () {
      this.globalNewVote = false
      this.globalNewVoteIndex = null
    }
  }
})
