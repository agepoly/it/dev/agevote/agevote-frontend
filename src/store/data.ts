import { defineStore } from 'pinia'
import API from '@/scripts/api'
import { ApiPromise, WsProvider } from '@polkadot/api'

import type { Ballot, DataStore, TellingToken, Ticket, User, VoteInformation, VoteResults } from '@/scripts/types'
import { apiInit, getVoteResults } from '@/scripts/blockchain'
import { useSessionStore } from './session'
import { DEPLOYMENTS, primaryBackends, randomBlockchainNode } from '@/scripts/embeddedConfig'
import { useAppStore } from './app'
import router from '@/router/index'
import { computeHashFromVote } from '@/scripts/crypto'

export const useDataStore = defineStore({
  id: 'data',

  state: (): DataStore => {
    const primaryBackendName = localStorage.getItem('primaryBackend')
    let primaryBackend = null
    if (primaryBackendName != null) {
      primaryBackend = DEPLOYMENTS.find(b => b.name === primaryBackendName)
    }

    const blockchainNodeName = localStorage.getItem('blockchainNode')
    let blockchainNode
    if (blockchainNodeName != null) {
      blockchainNode = DEPLOYMENTS.find(b => b.name === blockchainNodeName)
    }

    const savedBallotsStr = localStorage.getItem('saved-ballots')

    return {
      ticket: null,
      /// for users and operators (more data for the latter)
      users: [],
      primaryBackend: primaryBackend ?? primaryBackends()[0],
      blockchainNode: blockchainNode ?? randomBlockchainNode(),
      blockchainApi: null,
      votes: [],
      /// for operators, list of all tickets
      tickets: [],
      results: {},
      tellerKeys: undefined,
      tellingTokens: {},
      savedBallots: savedBallotsStr != null ? JSON.parse(savedBallotsStr) : {}
    }
  },

  getters: {
    ticketId (): number | null {
      return this.ticket?.id ?? null
    },
    isTicketBound (): boolean | null {
      return this.ticket?.bound ?? null
    },
    async bc (state): Promise<ApiPromise> {
      return await new Promise((resolve, reject) => {
        if (state.blockchainApi != null) {
          resolve(state.blockchainApi as ApiPromise)
          return
        }
        ApiPromise.create({ provider: new WsProvider(state.blockchainNode?.blockchainNodeUrl) }).then(api => {
          state.blockchainApi = api
          apiInit(api)
          resolve(api)
        }).catch(reject)
      })
    }
  },

  actions: {
    async refreshUsers (): Promise<void> {
      await new Promise((resolve, reject) => {
        if (useSessionStore().role === 'operator') {
          API.getAllUsers().then((users: User[]) => {
            this.users = users
            resolve(users)
          }).catch(err => {
            console.error('Failed to update users')
            reject(err)
          })
        } else {
          API.getAttendingUsers().then((users: User[]) => {
            this.users = users
            resolve(users)
          }).catch(err => {
            console.error('Failed to update users')
            reject(err)
          })
        }
      })
    },
    setUsers (users: User[]) {
      this.users = users
    },
    /// persist the state of the ticket (data + is it bound to the account ?)
    setTicket (id: number, code: string, bound: boolean) {
      this.ticket = { id, code, bound }
    },
    /// Called by the API after it has been released
    clearTicket () {
      this.ticket = null
    },
    async updateMyTicket (): Promise<void> {
      await new Promise<void>((resolve, reject) => {
        API.getMyTicket().then((serverTicket: Ticket | null) => {
          if (serverTicket != null) {
            console.log('Imported ticket from server ', serverTicket)
            this.ticket = { id: serverTicket.id, code: serverTicket.code, bound: true }
            resolve()
          } else {
            console.log("server doesn't have client's ticket : clearing ticket")
            this.ticket = null
            reject(new Error("server doesn't have client's ticket"))
          }
        }).catch(() => {
          this.ticket = null
          reject(new Error('API.getMyTicket failed'))
        })
      })
    },
    setBackend (type: 'primary' | 'blockchain', name: string) {
      const b = DEPLOYMENTS.find(b => b.name === name)
      if (b == null) return
      if (type === 'primary' && b.type !== 'secondary') {
        this.primaryBackend = b
        localStorage.setItem('primaryBackend', name)
      } else if (type === 'blockchain' && b.blockchainNodeUrl != null) {
        this.blockchainNode = b
        localStorage.setItem('blockchainNode', name)
      } else {
        console.error('Invalid choice', type, b)
      }
    },
    async refreshTickets (): Promise<Ticket[]> {
      return await new Promise((resolve, reject) => {
        if (useSessionStore().role === 'operator') {
          API.operatorGetAllTickets().then((tickets: Ticket[]) => {
            this.tickets = tickets
            resolve(tickets)
          }).catch(_err => {
            console.error('Failed to update users')
            reject(new Error())
          })
        }
      })
    },
    async refreshVotes (): Promise<void> {
      await new Promise((resolve, reject) => {
        API.getAllVotes().then((votes: VoteInformation[]) => {
          this.votes = votes.sort((a, b) => b.id - a.id)
          
          const openVote = this.votes.find(v => v.status === 'open')
          if (openVote != null) {
            // Purge old saved ballots
            const h = computeHashFromVote(openVote);
            const savedBallot: Ballot | undefined = this.savedBallots[openVote.id];
            if (savedBallot && savedBallot.form_hash != h) {
              console.log(`Purging old ballot with id ${openVote.id}. Previous form hash: ${savedBallot.form_hash} != new ${h}`);
              this.deleteBallot(openVote.id);
            }
            // New open vote notif logic
            const route = router.currentRoute.value
            if (route.name !== 'vote-form') {
              const app = useAppStore()
              app.newOpenVoteNotif(openVote.id)
            }
          }
          resolve(votes)
        }).catch(err => {
          reject(err)
        })
      })
    },
    addResults (voteIndex: number, ballots: Ballot[]) {
      this.results[voteIndex] = {
        ballots,
        validated: false
      }
    },
    validateResults (voteIndex: number) {
      const v = this.results[voteIndex]
      if (v != null) {
        v.validated = true
      }
    },
    async getResults (voteIndex: number): Promise<VoteResults> {
      return await new Promise((resolve, reject) => {
        const results = this.results[voteIndex]
        if (results != null) {
          resolve(results)
          return
        }
        getVoteResults(voteIndex).then(voteResults => {
          this.results[voteIndex] = voteResults
          resolve(voteResults)
        }).catch(reject)
      })
    },
    setTellerKeys (pubkey: string, key: string) {
      this.tellerKeys = { pubkey, key }
    },
    clearTellerKeys () {
      this.tellerKeys = undefined
    },
    saveTellingToken (voteIndex: number, timestamp: number, token: TellingToken) {
      if (this.tellingTokens[voteIndex] != null) {
        this.tellingTokens[voteIndex].push({ timestamp, token })
      } else {
        const arr = [{ timestamp, token }]
        console.log('adding array to tellingTokens map : ', arr)
        this.tellingTokens[voteIndex] = arr
        console.log('tellingTokens map : ', this.tellingTokens[voteIndex])
      }
    },
    clearTokens (voteIndex: number) {
      this.tellingTokens[voteIndex] = []
    },
    saveBallot (voteIndex: number, ballot: Ballot) {
      if (this.savedBallots[voteIndex] != null) {
        console.error(`A previous ballot already existed for the vote ${voteIndex}`, this.savedBallots[voteIndex], ballot)
        return
      }
      this.savedBallots[voteIndex] = ballot
      localStorage.setItem('saved-ballots', JSON.stringify(this.savedBallots))
    },
    deleteBallot (voteIndex: number) {
      if (this.savedBallots[voteIndex] == null) {
        console.error(`deleteBallot: ballot ${voteIndex} not found!`);
        return
      }
      delete this.savedBallots[voteIndex]
      localStorage.setItem('saved-ballots', JSON.stringify(this.savedBallots))
    }
  }
})
