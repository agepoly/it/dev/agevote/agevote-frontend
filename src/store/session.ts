import { defineStore } from 'pinia'

import type { SessionStore } from '@/scripts/types'

export const useSessionStore = defineStore({
  id: 'session',

  state: (): SessionStore => {
    const sessionStr = localStorage.getItem('session') ?? ''
    const session: SessionStore | null = (sessionStr.length > 0) ? JSON.parse(sessionStr) : null
    if (session != null) {
      console.log('session recovered from local storage :', session)
      return session
    }
    return {
      sciper: null,
      display_name: '',
      role: null,
      units: [],
      groups: [],
      exp: 0,
      jwt: null
    }
  },

  getters: {
    isActive (): boolean {
      if (this.sciper == null) return false

      const timestamp = Date.now() / 1000
      return timestamp < this.exp
    }
  },

  actions: {
    setSession (session: SessionStore) {
      Object.assign(this, session)
      localStorage.setItem('session', JSON.stringify(session))
    },
    clearSession () {
      localStorage.removeItem('session') // we don't want to remove the ticket code
      this.sciper = null
      console.log('Session cleared')
      window.location.reload()
    }
  }
})
