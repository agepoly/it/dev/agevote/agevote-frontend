/* eslint-disable @typescript-eslint/strict-boolean-expressions */
import { type RouteLocationNormalized, createRouter, createWebHistory, type RouteRecordRaw, type NavigationGuardNext } from 'vue-router'

import { useSessionStore } from '@/store/session'
import { useDataStore } from '@/store/data'

import Loading from '@/views/Loading.vue'

function checkSession (
  to: RouteLocationNormalized,
  from: RouteLocationNormalized,
  next: NavigationGuardNext
) {
  function nextWrapped (location: string | null) {
    const data = useDataStore()
    if (!data.ticket) {
      void data.updateMyTicket()
    }
    if (location) {
      next({ name: location })
    } else {
      next(true)
    }
  }

  const active = useSessionStore().isActive
  if (to.name === 'home' || to.name === 'info' || to.name === 'help') {
    active ? nextWrapped('votes') : next(true)
  } else {
    active ? nextWrapped(null) : next({ name: 'home' })
  }
}

function checkSessionWithTicket (
  to: RouteLocationNormalized,
  from: RouteLocationNormalized,
  next: NavigationGuardNext
) {
  const active = useSessionStore().isActive
  if (!active) {
    // ignore if no session
    next(true)
    return
  }

  useDataStore().updateMyTicket().finally(() => {
    next(true)
  })
}

const routes: RouteRecordRaw[] = [
  {
    path: '/',
    component: async () => await import('@/layouts/default/Default.vue'),
    children: [
      {
        path: '',
        name: 'home',
        component: async () => await import('@/views/Home.vue'),
        beforeEnter: checkSession
      },
      {
        path: 'info',
        name: 'info',
        component: async () => await import('@/views/Info.vue')
      },
      {
        path: 'help',
        name: 'help',
        component: async () => await import('@/views/Help.vue')
      },
      {
        path: 'users/attending',
        name: 'users-attending',
        component: async () => await import('@/views/users/UsersTable.vue'),
        props: { operatorMode: false }
      },
      {
        path: 'users',
        name: 'users-operator',
        component: async () => await import('@/views/users/UsersTable.vue'),
        props: { operatorMode: true }
      },
      {
        path: 'voting-right-overrides',
        name: 'voting-right-overrides',
        component: async () => await import('@/views/operators/VotingRightOverrides.vue'),
        props: { operatorMode: true }
      },
      {
        path: 'tickets',
        name: 'tickets-operator',
        component: async () => await import('@/views/operators/TicketsTable.vue'),
        props: { operatorMode: true }
      },
      {
        path: 'votes',
        name: 'votes',
        component: async () => await import('@/views/users/Votes.vue'),
        beforeEnter: checkSession
      },
      {
        path: 'votes/management',
        name: 'votes-management',
        component: async () => await import('@/views/operators/VotesManagement.vue'),
        beforeEnter: checkSession
      },
      {
        path: 'votes/:id/form',
        name: 'vote-form',
        component: async () => await import('@/views/users/VoteForm.vue'),
        beforeEnter: checkSession
      },
      {
        path: 'votes/:id/results',
        name: 'vote-results',
        component: async () => await import('@/views/users/VoteResults.vue'),
        beforeEnter: checkSession
      },
      {
        path: 'votes/:id/editor',
        name: 'vote-editor',
        component: async () => await import('@/views/operators/VoteEditor.vue'),
        beforeEnter: checkSession
      },
      {
        path: 'votes/none/teller-monitoring',
        name: 'teller-monitoring-selection',
        component: async () => await import('@/views/TellerMonitoringVoteSelection.vue'),
        beforeEnter: checkSession
      },
      {
        path: 'votes/:id/teller-monitoring',
        name: 'teller-monitoring',
        props: true,
        component: async () => await import('@/views/TellerMonitoring.vue'),
        beforeEnter: checkSession
      },
      {
        path: 'tequila/callback',
        component: Loading
      },
      {
        path: 'tickets/landing',
        name: 'tickets-landing',
        component: async () => await import('@/views/users/UserTicketManager.vue'),
        beforeEnter: checkSessionWithTicket
      },
      {
        path: 'presence/scanner',
        name: 'presence-scanner',
        component: async () => await import('@/views/presenceAgents/PresenceScanner.vue'),
        beforeEnter: checkSession
      },
      {
        path: 'presence/info/sciper',
        name: 'presence-info-sciper',
        component: async () => await import('@/views/presenceAgents/PresenceInfoSciper.vue'),
        beforeEnter: checkSession
      },
      {
        path: 'presence/search',
        name: 'presence-search',
        component: async () => await import('@/views/presenceAgents/PresenceSearch.vue'),
        beforeEnter: checkSession
      },
      {
        path: 'presence/serial',
        name: 'presence-serial',
        component: async () => await import('@/views/presenceAgents/PresenceSerial.vue'),
        beforeEnter: checkSession
      },
      {
        path: 'settings',
        name: 'settings',
        component: async () => await import('@/views/AdvancedSettings.vue')
      },
      {
        path: '/:pathMatch(.*)*',
        component: async () => await import('@/views/NotFound.vue')
      }
    ]
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
