/* tslint:disable */
/* eslint-disable */
/**
* @param {string} ballot_json
* @param {string} vote_key_str
* @param {string} telling_token_json
* @param {string} tellers_key
* @returns {EncryptedBallot}
*/
export function encrypt_ballot(ballot_json: string, vote_key_str: string, telling_token_json: string, tellers_key: string): EncryptedBallot;
/**
* @returns {string}
*/
export function random_seed(): string;
/**
* @param {string} s
* @returns {string}
*/
export function hash(s: string): string;
/**
* @returns {TellersKeyPair}
*/
export function generate_tellers_pair(): TellersKeyPair;
/**
* @param {string} cipher
* @param {string} tellers_key
* @returns {string}
*/
export function teller_decryption(cipher: string, tellers_key: string): string;
/**
*/
export class EncryptedBallot {
  free(): void;
/**
*/
  readonly encrypted_ballot: string;
/**
*/
  readonly encrypted_telling_token: string;
}
/**
*/
export class TellersKeyPair {
  free(): void;
/**
*/
  readonly key: string;
/**
*/
  readonly pubkey: string;
}

export type InitInput = RequestInfo | URL | Response | BufferSource | WebAssembly.Module;

export interface InitOutput {
  readonly memory: WebAssembly.Memory;
  readonly __wbg_encryptedballot_free: (a: number) => void;
  readonly encryptedballot_encrypted_ballot: (a: number, b: number) => void;
  readonly encryptedballot_encrypted_telling_token: (a: number, b: number) => void;
  readonly encrypt_ballot: (a: number, b: number, c: number, d: number, e: number, f: number, g: number, h: number, i: number) => void;
  readonly random_seed: (a: number) => void;
  readonly hash: (a: number, b: number, c: number) => void;
  readonly generate_tellers_pair: () => number;
  readonly teller_decryption: (a: number, b: number, c: number, d: number, e: number) => void;
  readonly __wbg_tellerskeypair_free: (a: number) => void;
  readonly tellerskeypair_pubkey: (a: number, b: number) => void;
  readonly tellerskeypair_key: (a: number, b: number) => void;
  readonly __wbindgen_add_to_stack_pointer: (a: number) => number;
  readonly __wbindgen_free: (a: number, b: number) => void;
  readonly __wbindgen_malloc: (a: number) => number;
  readonly __wbindgen_realloc: (a: number, b: number, c: number) => number;
  readonly __wbindgen_exn_store: (a: number) => void;
}

export type SyncInitInput = BufferSource | WebAssembly.Module;
/**
* Instantiates the given `module`, which can either be bytes or
* a precompiled `WebAssembly.Module`.
*
* @param {SyncInitInput} module
*
* @returns {InitOutput}
*/
export function initSync(module: SyncInitInput): InitOutput;

/**
* If `module_or_path` is {RequestInfo} or {URL}, makes a request and
* for everything else, calls `WebAssembly.instantiate` directly.
*
* @param {InitInput | Promise<InitInput>} module_or_path
*
* @returns {Promise<InitOutput>}
*/
export default function __wbg_init (module_or_path?: InitInput | Promise<InitInput>): Promise<InitOutput>;
