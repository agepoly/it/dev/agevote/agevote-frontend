/* tslint:disable */
/* eslint-disable */
export const memory: WebAssembly.Memory;
export function __wbg_encryptedballot_free(a: number): void;
export function encryptedballot_encrypted_ballot(a: number, b: number): void;
export function encryptedballot_encrypted_telling_token(a: number, b: number): void;
export function encrypt_ballot(a: number, b: number, c: number, d: number, e: number, f: number, g: number, h: number, i: number): void;
export function random_seed(a: number): void;
export function hash(a: number, b: number, c: number): void;
export function generate_tellers_pair(): number;
export function teller_decryption(a: number, b: number, c: number, d: number, e: number): void;
export function __wbg_tellerskeypair_free(a: number): void;
export function tellerskeypair_pubkey(a: number, b: number): void;
export function tellerskeypair_key(a: number, b: number): void;
export function __wbindgen_add_to_stack_pointer(a: number): number;
export function __wbindgen_free(a: number, b: number): void;
export function __wbindgen_malloc(a: number): number;
export function __wbindgen_realloc(a: number, b: number, c: number): number;
export function __wbindgen_exn_store(a: number): void;
