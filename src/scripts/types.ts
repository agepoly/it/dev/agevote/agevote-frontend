import type { ApiPromise } from '@polkadot/api'

/* -------- ERRORS -------- */

export enum AppError {
  InternalError,
  TequilaError,
  NetworkError,
  QRCodeError,
  CannotBindTicket,
  OperationFailed,
  OpenVoteFailed
}

export interface APIError {
  code: 'NotFound' | 'Forbidden' | 'IllegalOperation' | 'TequilaError' | 'Internal'

  /// for TequilaError and Internal
  msg?: string | null

  error?: {
    code: number
    reason: string
  }
}

/* -------- Stores -------- */

export interface AppStore {
  // Display
  navigationDrawer: boolean
  isDesktop: boolean
  // Global Message
  globalMessageModal: boolean
  globalMessageError: AppError | null
  globalMessageInfo: string | null
  globalMessageLoading: boolean
  // Global snackbar
  globalSnackbarDisplayed: boolean
  globalSnackbarText: string
  globalSnackbarType: 'success' | 'info' | 'error'
  globalNewVote: boolean
  globalNewVoteIndex: number | null
}

export type Role = null | 'operator' | 'teller' | 'presence_agent'

export interface SessionStore {
  sciper: string | null
  display_name: string
  role: Role
  units: string[]
  groups: string[]
  exp: number
  jwt: string | null
}

export interface DataStore {
  ticket: {
    id: number
    code: string
    bound: boolean
  } | null
  users: User[]
  primaryBackend: Backend
  blockchainNode: Backend
  blockchainApi: ApiPromise | null
  tickets: Ticket[]
  votes: VoteInformation[]
  results: Record<number, VoteResults>
  tellerKeys: { key: string, pubkey: string } | undefined
  tellingTokens: Record<number, Array<{ timestamp: number, token: TellingToken }>>
  savedBallots: Record<number, Ballot>
}

/* -------- VOTES -------- */

export type VoteStatus = 'prepared' | 'open' | 'closed' | 'validated' | 'archived' | 'canceled'

export interface VoteQuestion {
  question: string
  options: string[]
  min: number
  max: number
}

export interface VoteForm {
  title: string
  questions: VoteQuestion[]
  duration: number
  vote_pubkey: string | null
  teller_pubkey: string | null
}

export interface VoteInformation {
  id: number
  title: string
  questions: VoteQuestion[]
  duration: number
  vote_pubkey: string | null
  teller_pubkey: string | null
  status: VoteStatus
}

export interface VoteOnChainData {
  voteJson: string
  encryptedBallots: string
  encryptedTellingTokens: string
  results: string
  status: number
  validations: string
  openedAt: number
  duration: number
}

export interface Ballot {
  form_hash: string
  seed: string
  answers: boolean[][]
}

export interface TellingToken {
  ballot_hash: string
  sciper: string
  display_name: string
}

export interface VoteResults {
  ballots: Ballot[]
  validated: boolean
}

export function voteStatusFromNumber (status: number): VoteStatus | undefined {
  switch (status) {
    case 0:
      return 'prepared'
    case 1:
      return 'open'
    case 2:
      return 'closed'
    case 3:
      return 'validated'
    case 4:
      return 'archived'
    case 5:
      return 'canceled'
  }
}

export function voteStatusToNumber (status: VoteStatus): number {
  switch (status) {
    case 'prepared':
      return 0
    case 'open':
      return 1
    case 'closed':
      return 2
    case 'validated':
      return 3
    case 'archived':
      return 4
    case 'canceled':
      return 5
  }
}

export interface DKGStatus {
  dkg_present: boolean
  nodes: Record<string, {
    pubkey_present: boolean
    sent_deal_present: boolean
    sent_response_present: boolean
    share_present: boolean
    proxy: string
    state: Record<string, boolean>
    deals_received: boolean
    responses_received: boolean
  }>
  group_secret_present: boolean
  state: Record<string, boolean>
  name: string
  processed_resets: string
}

/* -------- USERS -------- */

export interface User {
  sciper: string
  displayName: string
  role?: string | null
  ticket?: string | null
}

export interface VotingRightOverride {
  sciper: string
  display_name: string
  description: string | null
}

/* -------- TICKETS -------- */

export interface Ticket {
  id: number
  code: string
  activated: boolean
  user_sciper?: string | null
}

export type BatchAction = 'activate' | 'deactivate' | 'clear_bindings' | 'destroy'

/* -------- INFRASTRUCTURE -------- */

export interface Backend {
  type: 'primary' | 'secondary' | 'test'
  env: 'prod' | 'debug' | 'staging'
  name: string
  backendUrl: string
  blockchainNodeUrl: string | undefined
}
