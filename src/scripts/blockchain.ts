import '@polkadot/api-augment'
import { type ApiPromise } from '@polkadot/api'
import type { Vec } from '@polkadot/types-codec'
import type { FrameSystemEventRecord } from '@polkadot/types/lookup'
import { useDataStore } from '@/store/data'
import { type VoteOnChainData, type Ballot, type VoteResults, type TellingToken, voteStatusFromNumber } from './types'
import { tellerDecryption } from './crypto'
import { useAppStore } from '@/store/app'
import { useRoute } from 'vue-router'

export function apiInit (api: ApiPromise) {
  void api.query.system.events((events) => { handleEvents(events) })
}

function handleEvents (events: Vec<FrameSystemEventRecord>) {
  events.forEach((record) => {
    const { event } = record

    if (event.section !== 'agevote') {
      console.log(`Received event ${event.method} from blockchain, section ${event.section} : ignored`)
      return
    }

    console.log('AGEVoté event from blockchain: ' + event.method, event)

    // Defer because the server might not have received the update yet
    setTimeout(() => {
      void useDataStore().refreshVotes()
    }, 1000)

    switch (event.method) {
      case 'VoteAdded':
        // data = {
        //   voteIndex: event.data[0],
        //   voteJson: event.data[1],
        //   status: event.data[2],
        //   who: event.data[3]
        // }
        handleVoteAdded(event.data[0].toJSON() as number, event.data[2].toJSON() as number)
        break
      case 'VoteStatusUpdated':
        // data = {
        //   voteIndex: event.data[0],
        //   oldStatus: event.data[1],
        //   newStatus: event.data[2],
        //   who: event.data[3]
        // }
        handleVoteStatusUpdated(event.data[0].toJSON() as number, event.data[1].toJSON() as number, event.data[2].toJSON() as number)
        break
      case 'VoteBallotAdded':
        // data = {
        //   voteIndex: event.data[0],
        //   encryptedTellingToken: event.data[1],
        //   who: event.data[2]
        // }
        handleVoteBallotAdded(event.data[0].toJSON() as number, event.data[1].toJSON() as string)
        break
      case 'VoteResultsAdded':
        // data = {
        //   voteIndex: event.data[0],
        //   encryptedBallots: event.data[1],
        //   results: event.data[2],
        //   who: event.data[3]
        // }
        handleVoteResultsAdded(event.data[0].toJSON() as number, event.data[2].toHex())
        break
      case 'VoteValidation':
        // data = {
        //   voteIndex: event.data[0],
        //   validations: event.data[1],
        //   who: event.data[2]
        // }
        break
    }
  })
}

export function hexToUTF8 (hex: string): string {
  if (hex.startsWith('0x')) {
    hex = hex.slice(2)
  }
  return decodeURIComponent(
    hex.replace(/\s+/g, '') // remove spaces
      .replace(/[0-9a-f]{2}/g, '%$&') // add '%' before each 2 characters
  )
}

export function hexToObject<T> (hex: string): T | null {
  const str = hexToUTF8(hex)
  try {
    return JSON.parse(str) as T
  } catch {
    return null
  }
}

function handleVoteAdded (voteIndex: number, _status: number) {
  const app = useAppStore()
  const route = useRoute()
  const status = voteStatusFromNumber(_status)
  const currentVoteIndex = parseInt(route.params.id as string)
  if (status === 'open' && currentVoteIndex !== voteIndex) {
    app.newOpenVoteNotif(voteIndex)
  }
}

function handleVoteResultsAdded (voteIndex: number, ballotsHex: string) {
  const ballots = hexToObject<Ballot[]>(ballotsHex)
  if (ballots == null) {
    console.error('Unable to parse VoteResultsAdded')
    return
  }
  useDataStore().addResults(voteIndex, ballots)
}

function handleVoteStatusUpdated (voteIndex: number, oldStatus: number, newStatus: number) {
  if (newStatus === 1) {
    const route = useRoute()
    if (route.name !== 'vote-form') {
      const app = useAppStore()
      app.newOpenVoteNotif(voteIndex)
    }
  } else if (oldStatus === 2 && newStatus === 3) {
    useDataStore().validateResults(voteIndex)
  }
}

export async function getVote (voteIndex: number): Promise<VoteOnChainData> {
  const data = useDataStore()
  return await new Promise((resolve, reject) => {
    data.bc.then(api => {
      api.query.agevote.votesMap(voteIndex).then(voteCodec => {
        if (voteCodec.isEmpty) {
          reject(new Error('not found'))
        } else {
          const vote = voteCodec.toJSON() as unknown as VoteOnChainData
          resolve(vote)
        }
      }).catch(reject)
    }).catch(reject)
  })
}

export async function getVoteResults (voteIndex: number): Promise<VoteResults> {
  return await new Promise((resolve, reject) => {
    getVote(voteIndex).then(vote => {
      const ballots = hexToObject<Ballot[]>(vote.results)
      if (ballots == null) { reject(new Error('parse error')); return }
      resolve({
        ballots,
        validated: vote.status === 3 || vote.status === 4
      })
    }).catch(reject)
  })
}

function handleVoteBallotAdded (voteIndex: number, encryptedTellingToken: string) {
  const data = useDataStore()
  const vote = data.votes.find(v => v.id === voteIndex)
  if (data.tellerKeys == null || vote == null) return
  if (vote.teller_pubkey !== data.tellerKeys.pubkey) {
    console.error(`The vote ${voteIndex} has a different teller pubkey than the one stored`)
    return
  }
  const key = data.tellerKeys.key
  const token = tellerDecryption(hexToUTF8(encryptedTellingToken), key)
  const timestamp = Date.now()
  console.log(`handleVoteBallotAdded: ${timestamp}`, token)
  data.saveTellingToken(voteIndex, timestamp, token)
}

export async function getTellingTokens (voteIndex: number): Promise<TellingToken[]> {
  return await new Promise((resolve, reject) => {
    const data = useDataStore()
    const vote = data.votes.find(v => v.id === voteIndex)

    if (data.tellerKeys == null || vote == null) {
      reject(new Error('missing key'))
      return
    }

    if (vote.teller_pubkey !== data.tellerKeys.pubkey) {
      console.error(`The vote ${voteIndex} has a different teller pubkey than the one stored`)
      reject(new Error('wrong teller key'))
      return
    }

    getVote(voteIndex).then(onChainVoteData => {
      data.clearTokens(voteIndex)
      console.log('onChainVoteData : ', onChainVoteData)
      const tokens: TellingToken[] = []
      for (const encToken of onChainVoteData.encryptedTellingTokens) {
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        const t = tellerDecryption(hexToUTF8(encToken), data.tellerKeys!.key)
        tokens.push(t)
        data.saveTellingToken(voteIndex, Date.now(), t)
      }
      resolve(tokens)
    }).catch(reject)
  })
}

export async function getVoteOpenedAt (voteIndex: number): Promise<number> {
  return await new Promise((resolve, reject) => {
    getVote(voteIndex).then(vote => {
      resolve(vote.openedAt)
    }).catch(reject)
  })
}
