import { type Backend } from './types'

export const DEPLOYMENTS: Backend[] = [
  { type: 'primary', env: 'prod', name: '[AGEPOLY] vote0', backendUrl: 'https://vote0.agepoly.ch', blockchainNodeUrl: 'wss://vote0.agepoly.ch/ws' },
  { type: 'primary', env: 'prod', name: '[AGEPOLY] vote1', backendUrl: 'https://vote0.agepoly.ch', blockchainNodeUrl: 'wss://vote1.agepoly.ch/ws' },
  { type: 'primary', env: 'prod', name: '[AGEPOLY] vote2', backendUrl: 'https://vote0.agepoly.ch', blockchainNodeUrl: 'wss://vote2.agepoly.ch/ws' },
  { type: 'primary', env: 'prod', name: '[AGEPOLY] vote3', backendUrl: 'https://vote0.agepoly.ch', blockchainNodeUrl: 'wss://vote3.agepoly.ch/ws' },
  
  { type: 'primary', env: 'staging', name: '[STAGING] vote0', backendUrl: 'https://vote0.ageptest.ch', blockchainNodeUrl: 'wss://vote0.ageptest.ch/ws' },
  { type: 'primary', env: 'staging', name: '[STAGING] vote1', backendUrl: 'https://vote0.ageptest.ch', blockchainNodeUrl: 'wss://vote1.ageptest.ch/ws' },
  { type: 'primary', env: 'staging', name: '[STAGING] vote2', backendUrl: 'https://vote0.ageptest.ch', blockchainNodeUrl: 'wss://vote2.ageptest.ch/ws' },
  { type: 'primary', env: 'staging', name: '[STAGING] vote3', backendUrl: 'https://vote0.ageptest.ch', blockchainNodeUrl: 'wss://vote3.ageptest.ch/ws' },

  // DISTRIBUTED PROD
  // { type: 'primary', env: 'prod', name: 'AGEPoly', backendUrl: 'https://agevote-bp.agepoly.ch', blockchainNodeUrl: 'wss://agevote-bc.agepoly.ch' },
  // { type: 'secondary', env: 'prod', name: 'PolyJapan', backendUrl: 'https://vote-agepoly.japan-impact.ch', blockchainNodeUrl: 'wss://block-agepoly.japan-impact.ch' },
  // { type: 'secondary', env: 'prod', name: 'Fréquence Banane', backendUrl: 'https://agevote.frequencebanane.ch', blockchainNodeUrl: 'wss://agevote.frequencebanane.ch:9944' },

  // TEST
  { type: 'test', env: 'debug', name: '[DEV] Vite Proxy', backendUrl: window.location.origin, blockchainNodeUrl: undefined },
  { type: 'test', env: 'debug', name: '[DEV] Same origin', backendUrl: `${window.location.protocol}//${window.location.hostname}:4000`, blockchainNodeUrl: undefined },
  { type: 'test', env: 'debug', name: '[DEV] Karatun', backendUrl: 'http://10.144.0.1:4000', blockchainNodeUrl: undefined },
  { type: 'test', env: 'debug', name: '[DEV] Karatun via HTTPS gateway', backendUrl: 'https://agevote-backend.loggerz.net', blockchainNodeUrl: undefined }
]

export function primaryBackends () {
  const debug = localStorage.getItem('debug') === 'true'
  return DEPLOYMENTS.filter(b => ((b.type === 'primary' && b.env === 'prod') || (debug && b.type !== 'secondary')) && b.backendUrl != null)
}

export function blockchainNodes () {
  const debug = localStorage.getItem('debug') === 'true'
  return DEPLOYMENTS.filter(b => (b.env === 'prod' || debug) && b.blockchainNodeUrl != null)
}

export function randomBlockchainNode () {
  const nodes = blockchainNodes()
  const i = Math.round(Math.random() * (nodes.length - 1))
  const bc = nodes[i]
  console.log('Selected blockchain node: ', bc)
  return bc
}
