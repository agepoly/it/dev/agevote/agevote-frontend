/* eslint-disable @typescript-eslint/naming-convention */
import wasm, { encrypt_ballot, random_seed, hash, generate_tellers_pair, teller_decryption, type EncryptedBallot, type TellersKeyPair } from '@/assets/wasm'
import type { Ballot, VoteForm, VoteInformation, TellingToken } from './types'

import { useSessionStore } from '@/store/session'

const initWasm = async () => {
  await wasm()
}
void initWasm()

export interface BallotReady {
  encryptedBallot: EncryptedBallot
  clearBallot: Ballot
}

export function computeHashFromVote(vote: VoteInformation): string {
  const form: VoteForm = {
    title: vote.title,
    questions: vote.questions,
    duration: vote.duration,
    vote_pubkey: vote.vote_pubkey,
    teller_pubkey: vote.teller_pubkey
  }
  return hash(JSON.stringify(form));
}

export function encryptBallot (answers: boolean[][], vote: VoteInformation): BallotReady {
  const session = useSessionStore()
  if (vote.vote_pubkey == null || vote.teller_pubkey == null || session.sciper == null || session.display_name == null) throw new Error('vote pubkey, teller pubkey, sciper or display_name is null')
  
  const ballot: Ballot = {
    form_hash: computeHashFromVote(vote),
    seed: random_seed(),
    answers
  }
  const ballotStr = JSON.stringify(ballot)

  const tellingToken: TellingToken = {
    sciper: session.sciper,
    display_name: session.display_name,
    ballot_hash: hash(ballotStr)
  }
  const tellingTokenStr = JSON.stringify(tellingToken)

  return {
    encryptedBallot: encrypt_ballot(ballotStr, vote.vote_pubkey, tellingTokenStr, vote.teller_pubkey),
    clearBallot: ballot
  }
}

export function generateTellerKeyPair (): TellersKeyPair {
  return generate_tellers_pair()
}

export function tellerDecryption (cipher: string, key: string): TellingToken {
  return JSON.parse(teller_decryption(cipher, key))
}
