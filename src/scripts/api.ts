/* eslint-disable @typescript-eslint/no-non-null-assertion */
/* eslint-disable @typescript-eslint/strict-boolean-expressions */
import { useAppStore } from '@/store/app'
import axios, { type AxiosError } from 'axios'
import { type APIError, AppError, type Role, type SessionStore, type User, type Ticket, type VoteInformation, type BatchAction, voteStatusFromNumber, type VoteStatus, voteStatusToNumber, type VoteForm, type DKGStatus, VotingRightOverride } from './types'
import { useSessionStore } from '@/store/session'
import { useDataStore } from '@/store/data'

export default {
  tequilaRequest,
  tequilaCallback,
  refreshJWT,
  getAttendingUsers,
  getAllUsers,
  getVotingRightOverrides,
  updateVotingRightOverride,
  updateUserRole,
  bindTicketSelf,
  unbindTicketSelf,
  operatorGetAllTickets,
  operatorUpdateTicketActivated,
  operatorUpdateTicketCode,
  operatorUpdateTicketSciper,
  getMyTicket,
  getAllVotes,
  createNewVote,
  operatorTicketsGeneration,
  operatorBatchAction,
  openVote,
  patchVote,
  deleteVote,
  updateVoteStatus,
  castBallot,
  updateTellerKey,
  resetDKG,
  debugDKG
}

/* ---------- LOGIN API ---------- */

interface PostTequilaRequestResponse {
  tequila_key: string
  auth_url: string
}

async function tequilaRequest (): Promise<void> {
  await new Promise((resolve, reject) => {
    primaryJson<PostTequilaRequestResponse>('/api/login/tequila-request', null).then(res => {
      window.location.href = res.auth_url
      resolve(null)
    }).catch((err: Error) => {
      if (err.message === 'ERR_API') {
        useAppStore().displayError(AppError.TequilaError)
      }
      reject(err)
    })
  })
}

interface UserSessionWithJWT {
  session: SessionStore
  jwt: string
}

async function tequilaCallback (key: string, authCheck: string): Promise<void> {
  await new Promise((resolve, reject) => {
    primaryJson<UserSessionWithJWT>('/api/login/tequila-callback', { request_key: key, auth_check: authCheck }, 'post').then(res => {
      res.session.jwt = res.jwt
      useSessionStore().setSession(res.session)
      resolve(res)
    }).catch((err: Error) => {
      if (err.message === 'ERR_API') {
        useAppStore().displayError(AppError.TequilaError)
      }
      reject(err)
    })
  })
}

/* ---------- USERS API ---------- */

async function refreshJWT (): Promise<void> {
  await new Promise((resolve, reject) => {
    primaryJson<UserSessionWithJWT>('/api/users/refresh-jwt', null, 'get').then(res => {
      res.session.jwt = res.jwt
      useSessionStore().setSession(res.session)
      resolve(res)
    }).catch((err: Error) => {
      if (err.message === 'ERR_API') {
        useAppStore().displayError(AppError.InternalError)
      }
      reject(err)
    })
  })
}

async function getAttendingUsers (): Promise<User[]> {
  return await new Promise((resolve, reject) => {
    primaryJson<User[]>('/api/users/attending', null, 'get').then(res => {
      useDataStore().setUsers(res)
      resolve(res)
    }).catch((err: Error) => {
      if (err.message === 'ERR_API') {
        useAppStore().displayError(AppError.InternalError)
      }
      reject(err)
    })
  })
}

async function getAllUsers (): Promise<User[]> {
  return await new Promise((resolve, reject) => {
    primaryJson<User[]>('/api/users', null, 'get').then(res => {
      useDataStore().setUsers(res)
      resolve(res)
    }).catch((err: Error) => {
      if (err.message === 'ERR_API') {
        useAppStore().displayError(AppError.InternalError)
      }
      reject(err)
    })
  })
}

async function updateUserRole (sciper: string, role: Role): Promise<void> {
  await new Promise((resolve, reject) => {
    primaryJson(`/api/users/${sciper}/role`, { role }).then(res => {
      resolve(res)
    }).catch((err: Error) => {
      if (err.message === 'ERR_API') {
        useAppStore().displayError(AppError.InternalError)
      }
      reject(err)
    })
  })
}

async function getVotingRightOverrides (): Promise<VotingRightOverride[]> {
  return await new Promise((resolve, reject) => {
    primaryJson<VotingRightOverride[]>('/api/users/voting_right_overrides', null, 'get').then(res => {
      resolve(res)
    }).catch((err: Error) => {
      if (err.message === 'ERR_API') {
        useAppStore().displayError(AppError.InternalError)
      }
      reject(err)
    })
  })
}

async function updateVotingRightOverride (sciper: string, overrideSet: boolean, description: string | null): Promise<void> {
  await new Promise((resolve, reject) => {
    primaryJson(`/api/users/${sciper}/voting_right_override`, { override_set: overrideSet, description }).then(res => {
      resolve(res)
    }).catch((err: Error) => {
      if (err.message === 'ERR_API') {
        useAppStore().displayError(AppError.InternalError)
      }
      reject(err)
    })
  })
}

/* ---------- TICKETS API - INFORMATION ---------- */

async function getMyTicket (): Promise<Ticket | null> {
  return await new Promise((resolve, reject) => {
    primaryJson<Ticket>('/api/tickets/my-ticket', null, 'get').then(res => {
      resolve(res)
    }).catch((err: Error) => {
      if (err.message === 'ERR_API') {
        const apiError = err.cause as APIError
        if (apiError.code === 'NotFound') {
          resolve(null)
        } else {
          useAppStore().displayError(AppError.InternalError)
          reject(err)
        }
      } else reject(err)
    })
  })
}

/* ---------- TICKETS API - TICKET BINDING ---------- */

async function bindTicketSelf (sciper: string, ticketId: number, code: string): Promise<void> {
  await new Promise((resolve, reject) => {
    primaryJson(`/api/tickets/${ticketId}/bind`, { code }).then(res => {
      useDataStore().setTicket(ticketId, code, true)
      resolve(res)
    }).catch((err: Error) => {
      if (err.message === 'ERR_API') {
        const apiError = err.cause as APIError
        if (apiError.code === 'IllegalOperation') {
          useAppStore().displayError(AppError.CannotBindTicket)
        } else {
          useAppStore().displayError(AppError.InternalError)
        }
      }
      reject(err)
    })
  })
}

// todo: presence agent bind

async function operatorUpdateTicketSciper (ticketId: number, sciper: string): Promise<void> {
  await new Promise((resolve, reject) => {
    primaryJson(`/api/tickets/${ticketId}/bind-op/${sciper}`, null).then(res => {
      resolve(res)
    }).catch((err: Error) => {
      if (err.message === 'ERR_API') {
        useAppStore().displayError(AppError.InternalError)
      }
      reject(err)
    })
  })
}

/* ---------- TICKETS API - TICKET DE-BINDING ---------- */

async function unbindTicketSelf (): Promise<void> {
  await new Promise((resolve, reject) => {
    primaryJson('/api/tickets/my-ticket/deactivate').then(res => {
      useDataStore().clearTicket()
      resolve(res)
    }).catch((err: Error) => {
      if (err.message === 'ERR_API') {
        const apiError = err.cause as APIError
        if (apiError.code === 'IllegalOperation') {
          useAppStore().displayError(AppError.OperationFailed)
        } else {
          useAppStore().displayError(AppError.InternalError)
        }
      }
      reject(err)
    })
  })
}

/* ---------- TICKETS API - ADVANCED TICKET MANIPULATION ---------- */

async function operatorUpdateTicketCode (ticketId: number, code: string): Promise<void> {
  await new Promise((resolve, reject) => {
    primaryJson(`/api/tickets/${ticketId}/update-code`, { code }).then(res => {
      resolve(res)
    }).catch((err: Error) => {
      if (err.message === 'ERR_API') {
        useAppStore().displayError(AppError.InternalError)
      }
      reject(err)
    })
  })
}

async function operatorGetAllTickets (): Promise<Ticket[]> {
  return await new Promise((resolve, reject) => {
    primaryJson<Ticket[]>('/api/tickets', null, 'get').then(res => {
      resolve(res)
    }).catch((err: Error) => {
      if (err.message === 'ERR_API') {
        useAppStore().displayError(AppError.InternalError)
      }
      reject(err)
    })
  })
}

async function operatorUpdateTicketActivated (ticketId: number, activated: boolean): Promise<void> {
  await new Promise((resolve, reject) => {
    primaryJson('/api/tickets/batch', { action: activated ? 'activate' : 'deactivate', from: ticketId, to: ticketId }).then(res => {
      resolve(res)
    }).catch((err: Error) => {
      if (err.message === 'ERR_API') {
        useAppStore().displayError(AppError.InternalError)
      }
      reject(err)
    })
  })
}

async function operatorBatchAction (action: BatchAction, from: number, to: number): Promise<void> {
  await new Promise((resolve, reject) => {
    primaryJson('/api/tickets/batch', { action, from, to }).then(res => {
      resolve(res)
    }).catch((err: Error) => {
      if (err.message === 'ERR_API') {
        useAppStore().displayError(AppError.InternalError)
      }
      reject(err)
    })
  })
}

async function operatorTicketsGeneration (quantity: number): Promise<void> {
  await new Promise((resolve, reject) => {
    primaryJson('/api/tickets/generate', { quantity }).then(res => {
      resolve(res)
    }).catch((err: Error) => {
      if (err.message === 'ERR_API') {
        useAppStore().displayError(AppError.InternalError)
      }
      reject(err)
    })
  })
}

/* VOTES API */

async function getAllVotes (): Promise<VoteInformation[]> {
  return await new Promise((resolve, reject) => {
    primaryJson<VoteInformation[]>('/api/votes', undefined, 'get').then(res => {
      // Conversion from number to VoteStatus
      for (const v of res) {
        const statusCast = voteStatusFromNumber(v.status as unknown as number)
        if (statusCast != null) {
          v.status = statusCast
        }
      }
      resolve(res)
    }).catch((err: Error) => {
      if (err.message === 'ERR_API') {
        console.log('ERROR === got error. :(')
        useAppStore().displayError(AppError.InternalError)
      }
      reject(err)
    })
  })
}

async function createNewVote (title: string): Promise<VoteInformation> {
  return await new Promise((resolve, reject) => {
    primaryJson<VoteInformation>('/api/votes', { title }, 'post').then(res => {
      resolve(res)
    }).catch((err: Error) => {
      if (err.message === 'ERR_API') {
        useAppStore().displayError(AppError.InternalError)
      }
      reject(err)
    })
  })
}

async function openVote (voteId: number): Promise<void> {
  await new Promise((resolve, reject) => {
    primaryJson(`/api/votes/${voteId}/open`).then(res => {
      resolve(res)
    }).catch((err: Error) => {
      if (err.message === 'ERR_API') {
        useAppStore().displayError(AppError.OpenVoteFailed)
      }
      reject(err)
    })
  })
}

async function patchVote (voteId: number, vote: VoteForm): Promise<VoteInformation> {
  return await new Promise((resolve, reject) => {
    primaryJson<VoteInformation>(`/api/votes/${voteId}`, vote, 'patch').then(res => {
      resolve(res)
    }).catch((err: Error) => {
      if (err.message === 'ERR_API') {
        useAppStore().displayError(AppError.InternalError)
      }
      reject(err)
    })
  })
}

async function deleteVote (voteId: number): Promise<void> {
  await new Promise((resolve, reject) => {
    primaryJson(`/api/votes/${voteId}/delete`).then(res => {
      resolve(res)
    }).catch((err: Error) => {
      if (err.message === 'ERR_API') {
        useAppStore().displayError(AppError.InternalError)
      }
      reject(err)
    })
  })
}

async function updateVoteStatus (voteId: number, status: VoteStatus): Promise<void> {
  await new Promise((resolve, reject) => {
    primaryJson(`/api/votes/${voteId}/status/${voteStatusToNumber(status)}`).then(res => {
      resolve(res)
    }).catch((err: Error) => {
      if (err.message === 'ERR_API') {
        useAppStore().displayError(AppError.InternalError)
      }
      reject(err)
    })
  })
}

async function castBallot (voteId: number, encryptedBallot: string, tellingToken: string): Promise<void> {
  await new Promise((resolve, reject) => {
    primaryJson(`/api/votes/${voteId}/ballot`, {
      encrypted_ballot: encryptedBallot,
      encrypted_telling_token: tellingToken
    }).then(res => {
      resolve(res)
    }).catch((err: Error) => {
      if (err.message === 'ERR_API') {
        useAppStore().displayError(AppError.InternalError)
      }
      reject(err)
    })
  })
}

async function updateTellerKey (tellerPubkey: string): Promise<void> {
  await new Promise((resolve, reject) => {
    primaryJson('/api/votes/teller-key', { pubkey: tellerPubkey }).then(res => {
      resolve(res)
    }).catch((err: Error) => {
      if (err.message === 'ERR_API') {
        useAppStore().displayError(AppError.InternalError)
      }
      reject(err)
    })
  })
}

async function resetDKG (voteId: number, type: 'public' | 'private'): Promise<void> {
  await new Promise((resolve, reject) => {
    primaryJson(`/api/dkg/${voteId}/reset-${type}`).then(res => {
      resolve(res)
    }).catch((err: Error) => {
      if (err.message === 'ERR_API') {
        useAppStore().displayError(AppError.InternalError)
      }
      reject(err)
    })
  })
}

async function debugDKG (voteId: number): Promise<DKGStatus> {
  return await new Promise((resolve, reject) => {
    primaryJson<DKGStatus>(`/api/dkg/${voteId}/debug-status`).then(res => {
      console.log(res)
      resolve(res)
    }).catch((err: Error) => {
      reject(err)
    })
  })
}

/* UTILITIES */

function unexpectedApiError (context: string, err?: unknown) {
  console.error(`[API - ${context}] Unexpected API Error. `, err)
}

async function primaryJson<T = unknown> (url: string, data?: unknown, method?: 'get' | 'post' | 'patch'): Promise<T> {
  url = useDataStore().primaryBackend.backendUrl + url

  return await new Promise((resolve, reject) => {
    const session = useSessionStore()
    axios(url, {
      headers: {
        Authorization: (session.jwt != null) ? (`Bearer ${session.jwt ?? ''}`) : null
      },
      data,
      method: method ?? 'post'
    }).then(async response => {
      if (response.status === 200) {
        if (response.headers['Content-Type'] === 'application/json' && response.data.length > 0) {
          try {
            const res = JSON.parse(response.data)
            resolve(res)
          } catch (_e) {
            unexpectedApiError('ERR_JSON')
            reject(Error())
          }
        } else resolve(response.data) // also handles T = null
      } else {
        try {
          const parsedErrorData = JSON.parse(response.data)
          console.log('Parsed ERR_API : ', parsedErrorData)
          reject(Error('ERR_API', { cause: parsedErrorData as APIError }))
        } catch {
          // We can't use non-JSON errors
          reject(Error())
        }
      }
    }).catch((err: AxiosError) => {
      if (err.response?.data) {
        try {
          const parsedErrorData = err.response.data as APIError
          if (parsedErrorData.error?.code === 403) {
            console.error('Error 403 -> clearSession & reload page')
            session.clearSession() // reloads the page
          } else {
            reject(Error('ERR_API', { cause: parsedErrorData }))
          }
        } catch {
          // We can't use non-JSON errors
          reject(Error(`ERR_CODE_${err.response?.status}`))
        }
      } else {
        unexpectedApiError('ERR_AXIOS', err)
        useAppStore().displayError(AppError.NetworkError, err.message)
        reject(Error('ERR_AXIOS', { cause: err }))
      }
    })
  })
}
